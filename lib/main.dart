import 'package:flutter/material.dart';

Column _builButtonColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(
        icon,
        color: color,
      ),
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 18,
            height: 1.5,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      )
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;

    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      color: Colors.blue[600],
      child: Row(
        children: [
          Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      'Tanwa Deesiri',
                      style: TextStyle(fontWeight: FontWeight.bold,fontSize: 50,color: Colors.white),
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Text(
                    'College Student',
                    style: TextStyle(fontWeight: FontWeight.bold,fontSize: 36,color: Colors.white,),
                    textAlign: TextAlign.start,

                  )
                ],
              )),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Text('Name: Tanwa Deesiri\n'
            'Age: 22\n'
            'Gender: Male\n'
            'E-mail: Ezakiyuuichii@gmail.com\n'
            'Phone: 080-xxx-xxxx\n'
            'Hobby: eat snack\n''ประวัติส่วนตัว :\n' '-เคยศึกษา มัธยมศึกษาที่ โรงเรียนหนองบอนวิทยาคม\n-ปัจจุบันศึกษาอยู่ที่ มหาวิทยาลัย'
            'บูรพา สาขาวิทยาการคอมพิวเตอร์ ปีที่ 4\n' 'Skill:',
          textAlign: TextAlign.start,
          style: TextStyle(height: 2.5,),
          softWrap: true,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "images/vue.png",
                width: 60,
                height: 36,
                fit: BoxFit.cover,
              ),
              Image.asset(
                "images/java.png",
                width: 60,
                height: 36,
                fit: BoxFit.cover,
              ),
              Image.asset(
                "images/html.png",
                width: 60,
                height: 36,
                fit: BoxFit.cover,
              ),
              Image.asset(
                "images/javascript.png",
                width: 60,
                height: 36,
                fit: BoxFit.cover,
              ),
              Image.asset(
                "images/bootstrap.png",
                width: 60,
                height: 36,
                fit: BoxFit.cover,
              ),
            ],
          )

        ],
      ),
    );

    return MaterialApp(
      title: 'Flutter layout demo',
      home: Scaffold(
        // appBar: AppBar(
        //   title: const Text('Flutter layout demo'),
        // ),
          body: ListView(
            children: [
              Image.asset(
                "images/best.png",
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              titleSection,
              textSection
            ],
          )),
    );
  }
}
